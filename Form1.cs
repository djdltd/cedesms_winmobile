using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Reflection;
using System.Windows.Forms;
using System.Collections;
using Microsoft.WindowsMobile.PocketOutlook;
using Microsoft.WindowsMobile.PocketOutlook.MessageInterception;
using System.Threading;

namespace SMSTest
{
    public partial class Form1 : Form
    {
        MessageInterceptor msgInterceptor;
        ArrayList alMessages = new ArrayList();
        delegate void StatusParameterDelegate(int status);
        SMSMessage lastOutgoingSMS = new SMSMessage();

        //const string HQPhoneNo = "651234567";
        Guid appID = new Guid("{283D3327-ADB8-41a0-B9DD-28E2845B5FF3}");


        public enum SMSType
        {
            Incoming = 0,
            Outgoing = 1
        }

        public enum SMSStatus
        {
            Sending = 0,
            Success = 1,
            Failure = 2
        }

        public Form1()
        {
            InitializeComponent();
        }

        public void AddSMSMessage (SMSMessage msg)
        {
            alMessages.Add(msg);
            RefreshMessageList();
        }

        public void SendSMSMessage(String strRecipient, String strMessage, String strPassword)
        {
            CipherLite enc = new CipherLite();

            String strEncrypted = enc.EncryptTextEx(strPassword, strMessage);

            lastOutgoingSMS = new SMSMessage(strRecipient, strEncrypted, (int) SMSType.Outgoing);

            AddSMSMessage(new SMSMessage(strRecipient, strEncrypted, (int)SMSType.Outgoing));

            Thread smsThread = new Thread(SMSJob);
            smsThread.Start();
        }

        public void SMSJob()
        {
            UpdateLastSMSStatus((int)SMSStatus.Sending);

            Thread.Sleep(2000);
            SmsSending.DestinationPort = 3590;
            int res = SmsSending.SendMessage(lastOutgoingSMS.mRecipient, lastOutgoingSMS.mMessage);

            if (res == 0)
            {
                UpdateLastSMSStatus((int)SMSStatus.Success);
            }
            else
            {
                UpdateLastSMSStatus((int)SMSStatus.Failure);
            }

            //MessageBox.Show("Send returned: ", res.ToString());
        }


        public void UpdateLastSMSStatus(int status)
        {
            if (InvokeRequired)
            {
                // We're not in the UI thread, so we need to call BeginInvoke
                BeginInvoke(new StatusParameterDelegate(UpdateLastSMSStatus), new object[] { status });
                return;
            }
            // Must be on the UI thread if we've got this far
            //statusIndicator.Text = value;
            UpdateSMSStatus(status);
        }

        public void UpdateSMSStatus(int status)
        {
            SMSMessage msg = new SMSMessage();

            msg = (SMSMessage)alMessages[alMessages.Count - 1];
            msg.mStatus = status;
            alMessages[alMessages.Count - 1] = msg;
            RefreshMessageList();

        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            /*
            try
            {
                SmsMessage sms = new SmsMessage();
                
                
                sms.Body = "Hello";
                sms.To.Add(new Recipient("07710978853:3590"));
                sms.Send();                                               

                MessageBox.Show("Message Sent!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
             * */
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //msgInterceptor = new MessageInterceptor(InterceptionAction.NotifyAndDelete, true);

            //msgInterceptor.MessageCondition = new MessageCondition(MessageProperty.Body, MessagePropertyComparisonType.StartsWith, "#CT", true);


            if (MessageInterceptor.IsApplicationLauncherEnabled(appID.ToString()))
            {
                msgInterceptor = new MessageInterceptor(appID.ToString(), true);
            }
            else
            {
                //---application has not been enabled yet; enable it now---
                msgInterceptor = new MessageInterceptor(InterceptionAction.NotifyAndDelete, true);

                //---set the filter for the message---
                msgInterceptor.MessageCondition = new MessageCondition(MessageProperty.Body, MessagePropertyComparisonType.StartsWith, "#CT", true);

                //---register the application---
                string appPath = Assembly.GetExecutingAssembly().GetName().CodeBase;
                
                msgInterceptor.EnableApplicationLauncher(appID.ToString(), appPath);
            }



            msgInterceptor.MessageReceived += new MessageInterceptorEventHandler(msgInterceptor_MessageReceived);


            alMessages.Add(new SMSMessage("01234777474", "This is a sample message", (int) SMSType.Incoming));


            RefreshMessageList();
        }

        public void RefreshMessageList()
        {
            int a = 0;
            SMSMessage curMessage = new SMSMessage();
            lstMessages.Items.Clear();

            for (a = 0; a < alMessages.Count; a++)
            {

                curMessage = (SMSMessage)alMessages[a];

                if (curMessage.mType == (int) SMSType.Incoming)
                {
                    lstMessages.Items.Add("From: " + curMessage.mRecipient);
                    //lstMessages.Font = new Font("Arial", 40, FontStyle.Bold);
                }
                else
                {
                    if (curMessage.mStatus == (int)SMSStatus.Sending)
                    {
                        lstMessages.Items.Add("To: " + curMessage.mRecipient + " (Sending...)");
                    }

                    if (curMessage.mStatus == (int)SMSStatus.Success)
                    {
                        lstMessages.Items.Add("To: " + curMessage.mRecipient + " (Success)");
                    }

                    if (curMessage.mStatus == (int)SMSStatus.Failure)
                    {
                        lstMessages.Items.Add("To: " + curMessage.mRecipient + " (Failed)");
                    }                    
                }               
            }
        }

        void msgInterceptor_MessageReceived(object sender, MessageInterceptorEventArgs e)
        {
            //---obtain the content of the SMS received---
            SmsMessage smsReceived = (SmsMessage)e.Message;

            //---add the message to the listbox control---
            //listBox1.Items.Add(smsReceived.Body);

            SMSMessage rcvmessage = new SMSMessage(smsReceived.From.Address, smsReceived.Body, (int) SMSType.Incoming);

            AddSMSMessage(rcvmessage);

            //MessageBox.Show("Encrypted Message Received.", "SMS Received", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
            notification1.Text = "Encrypted SMS Received from " + smsReceived.From.Address.ToString ();
            notification1.Caption = "Encrypted SMS Received.";
            notification1.InitialDuration = 10;
            notification1.Critical = false;
            notification1.Visible = true;


            //MessageBox.Show("Received: " + smsReceived.Body);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //SmsSending mysender = new SmsSending();

            //SmsSending.DestinationPort = 3590;
            //int res = SmsSending.SendMessage("07710978853", "Hello this is a test");

            //MessageBox.Show("Send returned: ", res.ToString());
            
        }

        private void menuItem2_Click(object sender, EventArgs e)
        {
            Compose compose = new Compose();
            compose.SetMainForm(this);
            compose.Show();
        }

        private void menuItem3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void lstMessages_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewMessage viewform = new ViewMessage();
            SMSMessage message = new SMSMessage();

            int index = lstMessages.SelectedIndex;
            if (index >= 0)
            {
                message = (SMSMessage)alMessages[index];

                viewform.setMessage(message.mMessage);
                viewform.setPhoneNumber(message.mRecipient);
            }

            viewform.Show();
        }

        private void menuItem4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

    }
}