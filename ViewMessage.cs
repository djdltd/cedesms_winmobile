using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SMSTest
{
    public partial class ViewMessage : Form
    {
        public ViewMessage()
        {
            InitializeComponent();
        }

        private bool IsExpired()
        {
            DateTime expTime = new DateTime(2009, 6, 10);
            if (DateTime.Now > expTime)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void menuItem4_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        public void setPhoneNumber(String strPhone)
        {
            txtPhone.Text = strPhone;
        }

        public void setMessage(String strMessage)
        {
            txtMessage.Text = strMessage;
        }

        private void menuItem2_Click(object sender, EventArgs e)
        {
            CipherLite enc = new CipherLite();
            String strEncrypted = txtMessage.Text;
            String strDecrypted = "";


            if (IsExpired() == false)
            {
                if (txtPassword.Text.Length > 3)
                {
                    strDecrypted = enc.DecryptTextEx(txtPassword.Text, strEncrypted);
                    txtMessage.Text = strDecrypted;
                }
                else
                {
                    MessageBox.Show("Password is too short.", "Password Error");
                }
            }
            else
            {
                MessageBox.Show("This version of CedeSMS has now expired. Please contact CedeSoft for further information.");
                this.Close();
            }
        }
    }
}