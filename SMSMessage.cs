using System;
using System.Collections.Generic;
using System.Text;

namespace SMSTest
{
    public class SMSMessage
    {
        public String mRecipient = "";
        public String mMessage = "";

        public enum SMSType
        {
            Incoming = 0,
            Outgoing = 1
        }

        public enum SMSStatus
        {
            Sending = 0,
            Success = 1,
            Failure = 2
        }

        public int mType = (int) SMSType.Incoming;
        public int mStatus = (int)SMSStatus.Sending;

        public SMSMessage(String strRecipient, String strMessage, int msgtype, int istatus)
        {
            mRecipient = strRecipient;
            mMessage = strMessage;
            mType = (int)msgtype;
            mStatus = (int)istatus;
        }

        public SMSMessage(String strRecipient, String strMessage, int msgtype)
        {
            mRecipient = strRecipient;
            mMessage = strMessage;
            mType = (int) msgtype;
        }

        public SMSMessage()
        {
            mRecipient = "";
            mMessage = "";
            mType = (int) SMSType.Incoming;
        }

    }
}
