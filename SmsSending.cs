using System;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using System.Data;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.IO;
using System.Net;
using System.Text;

namespace SMSTest
{
    public enum AddressType
    {
        Unknown,
        International,
        National,
        NetworkSpecific,
        Subscriber,
        Alphanumeric,
        Abbreviated
    }

    public struct PhoneAddress
    {
        public AddressType AddressType;
        public String Address;
    }

    public class SmsSending
    {
        private static string SMS_MSGTYPE_TEXT = "Microsoft Text SMS Protocol";
        private static long SMS_MODE_SEND = 0x00000002;
        private static long SMS_OPTION_DELIVERY_NONE = 0x00000000;
        private static long PS_MESSAGE_OPTION_DISCARD = 0x00000000;

        private enum PROVIDER_SPECIFIC_MESSAGE_CLASS
        {
            PS_MESSAGE_CLASS0 = 0,
            PS_MESSAGE_CLASS1,
            PS_MESSAGE_CLASS2,
            PS_MESSAGE_CLASS3,
        }

        private enum PROVIDER_SPECIFIC_REPLACE_OPTION
        {
            PSRO_NONE = 0,
            PSRO_REPLACE_TYPE1,
            PSRO_REPLACE_TYPE2,
            PSRO_REPLACE_TYPE3,
            PSRO_REPLACE_TYPE4,
            PSRO_REPLACE_TYPE5,
            PSRO_REPLACE_TYPE6,
            PSRO_REPLACE_TYPE7,
            PSRO_RETURN_CALL,
            PSRO_DEPERSONALIZATION,
        }

        unsafe private struct TEXT_PROVIDER_SPECIFIC_DATA
        {
            public IntPtr dwMessageOptions;
            public PROVIDER_SPECIFIC_MESSAGE_CLASS psMessageClass;
            public PROVIDER_SPECIFIC_REPLACE_OPTION psReplaceOption;
            public IntPtr dwHeaderDataSize;
            public fixed byte pbHeaderData[140];
            public bool fMessageContainsEMSHeaders;
            public IntPtr dwProtocolID;
        }

        private static int _srcPort;
        public static int SourcePort
        {
            get
            {
                return _srcPort;
            }
            set
            {
                _srcPort = value;
            }
        }

        private static int _dstPort;
        public static int DestinationPort
        {
            get
            {
                return _dstPort;
            }
            set
            {
                _dstPort = value;
            }
        }

        private enum SMS_DATA_ENCODING
        {
            SMSDE_OPTIMAL = 0,
            SMSDE_GSM,
            SMSDE_UCS2,
        }

        #region P/Invoke

        [DllImport("sms.dll")]
        private static extern IntPtr SmsOpen(String ptsMessageProtocol, IntPtr dwMessageModes, ref IntPtr psmshHandle, IntPtr phMessageAvailableEvent);


        [DllImport("sms.dll")]
        private static extern IntPtr SmsSendMessage(IntPtr smshHandle, IntPtr
        psmsaSMSCAddress, IntPtr psmsaDestinationAddress, IntPtr
        pstValidityPeriod, byte[] pbData, IntPtr dwDataSize, ref TEXT_PROVIDER_SPECIFIC_DATA pData,/* byte[]
pbProviderSpecificData, */
                                   IntPtr dwProviderSpecificDataSize,
        SMS_DATA_ENCODING smsdeDataEncoding, IntPtr dwOptions, IntPtr
        psmsmidMessageID);


        [DllImport("sms.dll")]
        private static extern IntPtr SmsClose(IntPtr smshHandle);

        #endregion

        unsafe public static int SendMessage(string sPhoneNumber, string sMessage)
        {

            IntPtr hSms = IntPtr.Zero;

            try
            {

                TEXT_PROVIDER_SPECIFIC_DATA tpsd;

                tpsd.dwMessageOptions = (IntPtr)PS_MESSAGE_OPTION_DISCARD;
                tpsd.psMessageClass = PROVIDER_SPECIFIC_MESSAGE_CLASS.PS_MESSAGE_CLASS1;
                tpsd.psReplaceOption = PROVIDER_SPECIFIC_REPLACE_OPTION.PSRO_NONE;
                tpsd.fMessageContainsEMSHeaders = false;
                tpsd.dwHeaderDataSize = (IntPtr)0x06;//specify the length of UDH
                tpsd.pbHeaderData[0] = Convert.ToByte(0x05);
                tpsd.pbHeaderData[1] = Convert.ToByte(0x04);//Port Format 16bits
                byte[] dest = BitConverter.GetBytes(_dstPort);
                tpsd.pbHeaderData[2] = dest[1];
                tpsd.pbHeaderData[3] = dest[0];
                byte[] src = BitConverter.GetBytes(_srcPort);
                tpsd.pbHeaderData[4] = src[1];
                tpsd.pbHeaderData[5] = src[0];
                tpsd.dwProtocolID = (IntPtr)0x00000000;

                IntPtr res = SmsOpen(SMS_MSGTYPE_TEXT, (IntPtr)SMS_MODE_SEND, ref hSms, IntPtr.Zero);
                if (res != IntPtr.Zero)
                    throw new Exception("Could not open SMS.");
                Byte[] bDest = new Byte[516];
                fixed (byte* pAddr = bDest)
                {
                    byte* pCurrent = pAddr;
                    Marshal.WriteInt32((IntPtr)pCurrent, (int)AddressType.Unknown);
                    pCurrent += 4;
                    foreach (byte b in Encoding.Unicode.GetBytes(sPhoneNumber))
                    {
                        Marshal.WriteByte((IntPtr)pCurrent, b);
                        pCurrent++;
                    }

                    byte[] bMessage = Encoding.Unicode.GetBytes(sMessage);


                    int nMsgSize = bMessage.Length;
                    res = SmsSendMessage(hSms, IntPtr.Zero, (IntPtr)pAddr, IntPtr.Zero, bMessage, (IntPtr)nMsgSize,
                    ref tpsd, (IntPtr)sizeof(TEXT_PROVIDER_SPECIFIC_DATA), SMS_DATA_ENCODING.SMSDE_GSM, (IntPtr)SMS_OPTION_DELIVERY_NONE, IntPtr.Zero);


                    if (res != IntPtr.Zero)
                        throw new Exception("SMS send failed.");

                    return 0;
                }
            }
            catch (Exception ee)
            {
                string strError = ee.Message;
                return -2;
            }
            finally
            {
                if (hSms != IntPtr.Zero)
                    SmsClose(hSms);
            }

        }


    }

}
