using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SMSTest
{
    public partial class Compose : Form
    {
        Form1 mainform;

        public Compose()
        {
            InitializeComponent();
        }

        public void SetMainForm (Form1 mform)
        {
            mainform = mform;
        }

        private bool IsExpired()
        {
            DateTime expTime = new DateTime(2009, 6, 10);
            if (DateTime.Now > expTime)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void mnuSendSMS_Click(object sender, EventArgs e)
        {
            //SmsSending mysender = new SmsSending();
            //SMSMessage msg = new SMSMessage (

            if (IsExpired () == false)
            {
                mainform.SendSMSMessage(txtPhone.Text, txtMessage.Text, txtPassword.Text);
                this.Close();
            }
            else
            {
                MessageBox.Show("This version of CedeSMS has now expired. Please contact CedeSoft for further information.");
                this.Close();
            }

            

            //SmsSending.DestinationPort = 3590;
            //int res = SmsSending.SendMessage("07710978853", "Hello this is a test");

            //MessageBox.Show("Send returned: ", res.ToString());

            /*
            try
            {
                SmsMessage sms = new SmsMessage();
                            
                            
                sms.Body = "Hello";
                sms.To.Add(new Recipient("07710978853:3590"));
                sms.Send();

                MessageBox.Show("Message Sent!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
             * */

        }

        private void mnuCancel_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void mnuCancel2_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}